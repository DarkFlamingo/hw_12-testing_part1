import CartParser from './CartParser';

let parser, parse, validate, parseLine, calcTotal;

beforeEach(() => {
  parser = new CartParser();
  parse = parser.parse.bind(parser);
  validate = parser.validate.bind(parser);
  parseLine = parser.parseLine.bind(parser);
  calcTotal = parser.calcTotal.bind(parser);
});

describe('CartParser - unit tests', () => {
  describe('Function validation', () => {
    it('should return error if header at least one of the headers is empty', () => {
      const data = `Product name,,Quantity`;
      expect(validate(data)).toEqual([
        {
          column: 1,
          row: 0,
          message: 'Expected header to be named "Price" but received .',
          type: 'header',
        },
      ]);
    });

    it('should return error if header at least one of the headers is incorrect', () => {
      const data = `Random name,Price,Quantity`;
      expect(validate(data)).toEqual([
        {
          column: 0,
          message:
            'Expected header to be named "Product name" but received Random name.',
          row: 0,
          type: 'header',
        },
      ]);
    });

    it('should return error if the columns in one of any rows are less than in the diagram', () => {
      const data = `Product name,Price,Quantity
                    Mollis consequat,
                    Tvoluptatem,10.32,1`;
      expect(validate(data)).toEqual([
        {
          message: 'Expected row to have 3 cells but received 2.',
          row: 1,
          column: -1,
          type: 'row',
        },
      ]);
    });

    it("should return error if cell with ColumnType='STRING' is empty", () => {
      const data = `Product name,Price,Quantity
                    ,9.00,2
                    Tvoluptatem,10.32,1`;
      expect(validate(data)).toEqual([
        {
          type: 'cell',
          message: 'Expected cell to be a nonempty string but received "".',
          row: 1,
          column: 0,
        },
      ]);
    });

    it("should return error if cell with ColumnType='NUMBER_POSITIVE' is a string", () => {
      const data = `Product name,Price,Quantity
                    Mollis consequat,9.00,2,
                    Tvoluptatem,10.32,Opps I'm String`;
      expect(validate(data)).toEqual([
        {
          type: 'cell',
          column: 2,
          row: 2,
          message:
            'Expected cell to be a positive number but received "Opps I\'m String".',
        },
      ]);
    });

    it("should return error if cell with ColumnType='NUMBER_POSITIVE' is negative number", () => {
      const data = `Product name,Price,Quantity
                    Mollis consequat,9.00,-2,
                    Tvoluptatem,10.32,1`;
      expect(validate(data)).toEqual([
        {
          type: 'cell',
          column: 2,
          row: 1,
          message: 'Expected cell to be a positive number but received "-2".',
        },
      ]);
    });

    it("should return error if cell with ColumnType='NUMBER_POSITIVE' is 'Infinity'", () => {
      const data = `Product name,Price,Quantity
                    Mollis consequat,9.00,'Infinity',
                    Tvoluptatem,10.32,1`;
      expect(validate(data)).toEqual([
        {
          type: 'cell',
          column: 2,
          row: 1,
          message:
            'Expected cell to be a positive number but received "\'Infinity\'".',
        },
      ]);
    });

    it('should return empty errors array if data is valid', () => {
      const data = `Product name,Price,Quantity
                    Mollis consequat,9.00,2
                    Tvoluptatem,10.32,1
                    Scelerisque lacinia,18.90,1
                    Consectetur adipiscing,28.72,10
                    Condimentum aliquet,13.90,1`;
      expect(validate(data).length).toEqual(0);
    });
  });
  describe('Function parseLine', () => {
    it('should return null if line is empty', () => {
      const data = `Mollis consequat,9.00,2`;
      expect(parseLine(data)).toMatchObject({
        name: 'Mollis consequat',
        price: 9.0,
        quantity: 2,
      });
    });
  });
  describe('Function calcTotal', () => {
    it('should return correct number', () => {
      const data = [
        { price: 3.4, quantity: 54 },
        { price: 89.2, quantity: 32 },
      ];
      expect(calcTotal(data).toFixed(1)).toBe('3038.0');
    });
  });
  describe('Function parse', () => {
    it('should throw error if errors array is not empty', () => {
      parser.readFile = jest.fn(
        () => `Product name,Price,Quantity
              Tvoluptatem,10.32,1
              
              Scelerisque lacinia,18.90,1
              Consectetur adipiscing,28.72,10
              Condimentum aliquet,13.90,1`
      );
      parser.validate = jest.fn(() => [
        {
          type: 'row',
          row: 2,
          column: -1,
          message: 'Expected row to have 3 cells but received 1.',
        },
      ]);
      expect(() => parse('')).toThrowError('Validation failed!');
    });
  });
});

describe('CartParser - integration test', () => {
  it('should return the correct result of the program', () => {
    const data = './samples/cart.csv';
    expect(parse(data)).toMatchObject({
      items: [
        {
          name: 'Mollis consequat',
          price: 9,
          quantity: 2,
        },
        {
          name: 'Tvoluptatem',
          price: 10.32,
          quantity: 1,
        },
        {
          name: 'Scelerisque lacinia',
          price: 18.9,
          quantity: 1,
        },
        {
          name: 'Consectetur adipiscing',
          price: 28.72,
          quantity: 10,
        },
        {
          name: 'Condimentum aliquet',
          price: 13.9,
          quantity: 1,
        },
      ],
      total: 348.32,
    });
  });
});
